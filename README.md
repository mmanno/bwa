# Example of using gitlab continuous integration and deployment CI/CD
# https://docs.gitlab.com/ee/ci/

# bwa Singularity container
Bionformatics package bwa<br>
BWA is a software package for mapping DNA sequences against a large reference genome, such as the human genome. It consists of three algorithms: BWA-backtrack, BWA-SW and BWA-MEM.
https://github.com/lh3/bwa
bwa Version: 0.7.17<br>

1) Create a folder with these 3 files:<br>
.gitlab-ci.yml<br>
README.md<br>
bwa_v0.7.17.def<br>

2) run:<br>
`git init`<br>
`git remote rm origin`<br>
`git remote add origin https://forgemia.inra.fr/<yourgitlabaccount>/bwa.git`<br>
`git add .`<br>
`git commit -m "Initial commit"`<br>
`git remote -v`<br>
`git push -u origin master`<br>


Singularity container based on the recipe: bwa_v0.7.17.def

Package installation using Miniconda3 V4.7.12<br>

image singularity (V>=3.3) is automaticly built (gitlab-ci) and pushed on the registry using the .gitlab-ci.yml <br>

can be pull (singularity version >=3.3) with:<br>
`singularity pull bwa_v0.7.17.sif oras://registry.forgemia.inra.fr/<yourgitlabaccount>/bwa/bwa:latest`

Or you can download directly the singularity-image: artefacts.zip
